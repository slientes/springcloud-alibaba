package com.jiayou.api

import feign.hystrix.FallbackFactory
import org.springframework.stereotype.Component
import java.lang.Exception

/**
@author 李 双
@data   2021-04-01  20:59
@info
 */

@Component
class DepartFallbackFactory : FallbackFactory<ProviderService> {
    override fun create(p0: Throwable): ProviderService =
            object : ProviderService {
                override fun port(): String = "port异常！"

                override fun index(): String = "index异常！"

                override fun seata1() = "seata1异常"

                override fun seata2() = throw Exception("\"seata2异常\"")


            }
}
