package com.jiayou

import cn.hutool.core.date.DateUtil
import com.alibaba.csp.sentinel.slots.block.BlockException
import kotlin.reflect.full.declaredMemberFunctions

/**
@author 李 双
@data   2021-03-31  17:39:14
@info
 */
object FallBackService {
    @JvmStatic
    fun blockHandler(blockException: BlockException) =
            "FallBackService处理！".also { println(DateUtil.now()) }
}

