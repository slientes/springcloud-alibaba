package com.jiayou

import cn.hutool.json.JSONUtil
import com.alibaba.csp.sentinel.adapter.gateway.sc.callback.GatewayCallbackManager
import com.jiayou.autoconfiguration.AuthAutoConfiguration
import com.jiayou.common.Constant
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.gateway.filter.GatewayFilter
import org.springframework.cloud.gateway.filter.GatewayFilterChain
import org.springframework.cloud.gateway.filter.GlobalFilter
import org.springframework.context.annotation.Bean
import org.springframework.core.Ordered
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.body
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono
import javax.annotation.PostConstruct

/**
@author 李 双
@data   2021-04-01  11:57:47
@info   网关主启动类（排除openfeign的请求头转发和特征验证）
 */

@EnableScheduling
@EnableDiscoveryClient
@SpringBootApplication(exclude = [AuthAutoConfiguration::class])
class GateWayApplication {

    //注入限流handler
    @PostConstruct
    fun sentinelBlockHandler() {
        GatewayCallbackManager.setBlockHandler { serverWebExchange, throwable ->
            ServerResponse.status(200).syncBody(Constant.gateWayLimiting())
        }
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            runApplication<GateWayApplication>(*args)
        }
    }
}