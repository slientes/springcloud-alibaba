package com.jiayou.autoconfiguration

import com.jiayou.common.AuthFilter
import com.jiayou.common.AuthHandler
import com.jiayou.common.FeignInterceptor
import feign.RequestInterceptor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.cloud.client.ConditionalOnReactiveDiscoveryEnabled
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import javax.servlet.Filter
import javax.servlet.http.HttpServletRequest

/**
@author 李 双
@data   2021-04-02  13:39:05
@info   配置feign和filter
 */
@Configuration
@EnableConfigurationProperties(AuthProperties::class)
class AuthAutoConfiguration {


    //鉴权过滤器，如果没有鉴权处理程序，该过滤器不会加入过滤器链
    @Bean
    @ConditionalOnBean(AuthHandler::class)
    fun authFilter(authHandler: AuthHandler, authProperties: AuthProperties): FilterRegistrationBean<Filter> {
        val filterRegistrationBean = FilterRegistrationBean<Filter>();
        filterRegistrationBean.filter = AuthFilter(authHandler, authProperties)
        filterRegistrationBean.addUrlPatterns("/*");
        return filterRegistrationBean;
    }

    //feign拦截器，在微服务间远程调用时传递凭据以及cookie
    @Bean
    @ConditionalOnWebApplication
    fun feignInterceptor(httpServletRequest: HttpServletRequest): FeignInterceptor {
        return FeignInterceptor(httpServletRequest)
    }

}