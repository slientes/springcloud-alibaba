package com.jiayou.api

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.RequestMapping

/**
@author 李 双
@data   2021-04-01  12:48:38
@info
 */
@Component
@FeignClient("provider",fallbackFactory = DepartFallbackFactory::class)
interface ProviderService {

    @RequestMapping("port")
    fun port(): String


    @RequestMapping("index")
    fun index(): String

    @RequestMapping("seata1")
    fun seata1(): String

    @RequestMapping("seata2")
    fun seata2(): String
}