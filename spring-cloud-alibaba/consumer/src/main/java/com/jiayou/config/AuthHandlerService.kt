package com.jiayou.config

import cn.hutool.crypto.digest.BCrypt
import com.jiayou.autoconfiguration.AuthProperties
import com.jiayou.common.AuthHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.ComponentScan
import org.springframework.data.redis.core.StringRedisTemplate
import org.springframework.stereotype.Component

/**
@author 李 双
@data   2021-04-02  12:46:15
@info   配置请求拦截
 */
@Component
class AuthHandlerService : AuthHandler {

    @Autowired
    private lateinit var redisTemplate: StringRedisTemplate

    @Autowired
    private lateinit var authProperties: AuthProperties


    override fun doAuth(auth: String) = BCrypt.checkpw(redisTemplate.opsForValue()[authProperties.code], auth)
}