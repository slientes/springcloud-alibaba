package com.jiayou.slient.service;

import com.jiayou.slient.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lishuang
 * @since 2021-04-05
 */
interface IUserService : IService<User>
