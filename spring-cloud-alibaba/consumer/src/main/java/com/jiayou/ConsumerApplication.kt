package com.jiayou


import com.alibaba.cloud.nacos.ribbon.NacosRule
import com.alibaba.csp.sentinel.annotation.SentinelResource
import com.jiayou.api.ProviderService
import com.jiayou.config.FallBackService
import com.jiayou.slient.entity.User
import com.jiayou.slient.service.impl.UserServiceImpl
import io.seata.core.context.RootContext
import io.seata.rm.datasource.DataSourceProxy
import io.seata.spring.annotation.GlobalTransactional
import io.seata.tm.api.GlobalTransactionContext
import org.mybatis.spring.annotation.MapperScan
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.context.annotation.Bean
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpSession
import javax.sql.DataSource


@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@RestController
@MapperScan("com.jiayou.slient.mapper")
class ConsumerApplication {

    @Bean
    fun iRule() = NacosRule()

    @Autowired
    private lateinit var providerService: ProviderService

    @Autowired
    private lateinit var userServiceImpl: UserServiceImpl


    @ResponseBody
    @RequestMapping("port")
    @SentinelResource("port", blockHandlerClass = [FallBackService::class], blockHandler = "blockHandler")
    fun port(httpServletResponse: HttpServletResponse, httpServletRequest: HttpServletRequest) = providerService.port()


    @ResponseBody
    @RequestMapping("index")
    fun index(httpSession: HttpSession) = providerService.index()


    @ResponseBody
    @RequestMapping("seata1")
    @GlobalTransactional
    fun seata1(): String {
        val user = User()
        user.id=1;
        user.password = "10086"
        userServiceImpl.updateById(user)
        return providerService.seata1()
    }

    @ResponseBody
    @RequestMapping("seata2")
    @GlobalTransactional
    fun seata2(): String {
        val user = User()
        user.id=1
        user.password = "10000"
        userServiceImpl.updateById(user)
        return providerService.seata2()
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            runApplication<ConsumerApplication>(*args)
        }
    }
}

