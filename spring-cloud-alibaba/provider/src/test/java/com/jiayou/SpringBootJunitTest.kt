package com.jiayou

import com.jiayou.slient.service.impl.UserServiceImpl
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

/**
@author 李 双
@data   2021-04-05  16:47
@info
 */

@SpringBootTest
class SpringBootJunitTest {

    @Autowired
    private lateinit var userServiceImpl: UserServiceImpl

    @Test
    fun test() {
        userServiceImpl.list().forEach { println(it) }
    }


}