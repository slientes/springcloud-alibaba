package com.jiayou.common

import cn.hutool.json.JSONUtil
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.ComponentScans
import javax.servlet.http.HttpServletResponse

/**
@author 李 双
@data   2021-04-02  12:36:18
@info   常用方法
 */
object Constant {

    @JvmStatic
    fun accessDenied(message: String = "非法访问", httpServletResponse: HttpServletResponse) {
        httpServletResponse.status = 401
        httpServletResponse.characterEncoding = "UTF-8"
        httpServletResponse.contentType = "application/json;charset=utf-8"
        httpServletResponse.writer.write(JSONUtil.toJsonPrettyStr("{'code':401,message:'${message}'}"))
    }

    @JvmStatic
    fun accessAuthorizedDenied(message: String = "您没有权限访问该资源", httpServletResponse: HttpServletResponse) {
        httpServletResponse.status = 403
        httpServletResponse.characterEncoding = "UTF-8"
        httpServletResponse.contentType = "application/json;charset=utf-8"
        httpServletResponse.writer.write(JSONUtil.toJsonPrettyStr("{'code':403,message:'${message}'}"))
    }

    @JvmStatic
    fun gateWayLimiting(): String = JSONUtil.parse("{'code':201,'message':'网关限流，请稍后再试','data':'null'}").toJSONString(2)


}