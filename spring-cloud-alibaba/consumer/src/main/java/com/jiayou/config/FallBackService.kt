package com.jiayou.config

import com.alibaba.csp.sentinel.slots.block.BlockException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
@author 李 双
@data   2021-04-01  16:16:20
@info
 */
object FallBackService {

    @JvmStatic
    fun blockHandler(httpServletResponse: HttpServletResponse?, httpServletRequest: HttpServletRequest?,blockException: BlockException?)
    = "FallBackService处理！"
}

