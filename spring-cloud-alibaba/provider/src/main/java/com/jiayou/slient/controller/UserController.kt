package com.jiayou.slient.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lishuang
 * @since 2021-04-05
 */
@RestController
@RequestMapping("/slient/user")
class UserController

