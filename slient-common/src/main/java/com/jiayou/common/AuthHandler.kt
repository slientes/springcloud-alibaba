package com.jiayou.common

/**
@author 李 双
@data   2021-04-02  11:55:14
@info   鉴权抽象
 */
interface AuthHandler {
    fun doAuth(auth: String): Boolean
}