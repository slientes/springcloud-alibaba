package com.jiayou.slient.service.impl;

import com.jiayou.slient.entity.User;
import com.jiayou.slient.mapper.UserMapper;
import com.jiayou.slient.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lishuang
 * @since 2021-04-05
 */
@Service
  class UserServiceImpl : ServiceImpl<UserMapper, User>(), IUserService {

}
