package com.jiayou.slient.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;


/**
 * <p>
 *
 * </p>
 *
 * @author lishuang
 * @since 2021-04-05
 */

data class User(
        @TableId(value = "id", type = IdType.AUTO)
        var id: Int? = null,
        var username: String? = null,
        var password: String? = null,
        var age: Int? = null
)
