package com.jiayou

import cn.hutool.core.date.DateUtil
import cn.hutool.core.util.RandomUtil
import com.alibaba.csp.sentinel.annotation.SentinelResource
import com.jiayou.slient.entity.User
import com.jiayou.slient.service.impl.UserServiceImpl
import io.seata.core.context.RootContext
import io.seata.rm.datasource.DataSourceProxy
import io.seata.tm.api.GlobalTransactionContext
import org.mybatis.spring.annotation.MapperScan
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean
import org.springframework.boot.runApplication
import org.springframework.boot.web.servlet.ServletComponentScan
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.context.annotation.Bean
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.lang.Exception
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpSession
import javax.sql.DataSource

/**
@author 李 双
@data   2021-04-01  12:50:31
@info
 */

@RestController
@EnableDiscoveryClient
@SpringBootApplication
@MapperScan("com.jiayou.slient.mapper")
class ProviderApplication {

    @Value("${'$'}{server.port}")
    private lateinit var port: String

    @Autowired
    private lateinit var userServiceImpl: UserServiceImpl

    @SentinelResource("p", blockHandlerClass = [FallBackService::class], blockHandler = "blockHandler")
    @RequestMapping("port")
    fun port(httpServletRequest: HttpServletRequest) = port

    @RequestMapping("index")
    fun index(): String {
        return "index -> $port"
    }



    @RequestMapping("seata1")
    fun seata1(): String {
        userServiceImpl.updateById(User(id = 2, password = "10086"))
        return "分布式事务测试通过！"
    }

    @RequestMapping("seata2")
    fun seata2(): String {

        userServiceImpl.updateById(User(id = 2, password = "10000"))
        throw Exception()
        return "分布式事务测试通过！"
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            runApplication<ProviderApplication>(*args)
        }
    }
}