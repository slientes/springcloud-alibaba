package com.jiayou.autoconfiguration

import org.springframework.boot.context.properties.ConfigurationProperties
import java.io.Serializable

/**
@author 李 双
@data   2021-04-02  14:57:32
@info   配置特征标记
 */
@ConfigurationProperties(prefix = "auth")
data class AuthProperties(var code: String = "auth_access", var filterPath: List<String> = listOf("/*")) : Serializable
