package com.jiayou.slient.mapper;

import com.jiayou.slient.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lishuang
 * @since 2021-04-05
 */
interface UserMapper : BaseMapper<User>
