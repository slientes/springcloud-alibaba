package com.jiayou.common

import feign.RequestInterceptor
import feign.RequestTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.*
import javax.servlet.http.HttpServletRequest

/**
@author 李 双
@data   2021-04-02  11:13:02
@info   OpenFeign拦截器
 */

class FeignInterceptor(private val httpServletRequest: HttpServletRequest) : RequestInterceptor {
    override fun apply(request: RequestTemplate) {
        //转发所有header
        val headerNames = httpServletRequest.headerNames
        Optional.ofNullable(headerNames).ifPresent {
            while (it.hasMoreElements()) {
                val element = it.nextElement()
                request.header(element, httpServletRequest.getHeader(element))
            }
        }
    }
}
