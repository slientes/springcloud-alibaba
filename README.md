# springcloud-alibaba

#### 介绍

**分布式架构脚手架**

#### 软件架构
 基于springcloud+springcloud alibaba中间件集成

**分布式组件：**

- seata1.4.1（分布式事务）
- nacos1.4.1（注册中心、配置中心）
- springcloud-alibaba-sentinel（熔断、限流）
- redis（缓存、校验）
- openfeign（远程调用）
- sentinel-gateway（sentinel整个gateway作为服务网关）
- zipkin（整合sleuth、链路追踪）

**基于JDK8 kotlin1.4.21编译**

内置一个鉴权服务和一个特征码转发服务：

​	鉴权服务：所有服务必须通过网关转发，各个微服务都自动装配一个特征值验证的过滤器，判断该请求是否为网关转发而来，借用redis通过加盐非对称算法加密校验，非网关转发的请求直接拒绝响应。

​	特征码转发服务：通过gateway的请求的header会被gateway变异一个特征值header（由后台配置指定，不配置默认为auth_acess），每个微服务都会校验这个特征值，失败则直接拒绝服务，链路调用中，会有： 服务A ->服务 B ->服务 C ->服务D的情况，这里采取feign转发特征值header来保证特征值的传递。



 

![](C:\Users\slient\Desktop\Alibaba\分布式基础架构.png)



![](https://www.jiayou.art/gitee/分布式基础架构.png)